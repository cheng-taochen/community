package com.example.community.util;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.DigestUtils;

import java.util.Map;
import java.util.UUID;

/**
 * @Author: Ctc
 * @Date: 2022/3/29 12:19
 */
public class CommunityUtil {
    //生成随机字符串
    public static String generateUUID() {
        //把随机生成的-替换为
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    //MD5加密
    //hello->abc123def456 每次加密都是这次
    //hello+3e4a8(盐)->
    //key 是原始密码+盐,然后对字符串进行加密
    public static String md5(String key) {
        //看是否为空值
        if (StringUtils.isAllBlank(key)) {
            return null;
        }
        //Spring自带的加密工具
        //加密结果加密为16位返回
        //传入的参数值为byte类型
        return DigestUtils.md5DigestAsHex(key.getBytes());
    }

    public static String getJSONString(int code, String msg, Map<String, Object> map) {
        JSONObject json = new JSONObject();
        json.put("code", code);
        json.put("msg", msg);
        if (map != null) {
            for (String key : map.keySet()) {
                json.put(key, map.get(key));
            }
        }
        return json.toJSONString();
    }

    public static String getJSONString(int code, String msg) {
        return getJSONString(code, msg, null);
    }

    public static String getJSONString(int code) {
        return getJSONString(code, null, null);
    }


}
