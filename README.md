部署：配置mysql-》开启redis-》开启kafka（先开启zookeeper）-》开启ElaticSearch-》启动项目 


**第1章
初识Spring Boot，开发社区首页** 


- 搭建开发环境
- Spring入门
- Spring MVC入门
- MyBatis入门
- 开发社区首页
- 项目调试技巧
- 版本控制



 **第2章
Spring Boot实践，开发社区登陆模块** 


- 发送邮件
- 开发注册功能
- 会话管理
- 生成验证码
- 开发登陆、退出功能
- 显示登陆信息
- 账号设置
- 检查登录状态




 **第3章
Spring Boot进阶，开发社区核心功能** 


- 过滤敏感词
- 发布帖子
- 帖子详情
- 事务管理
- 显示评论
- 添加评论
- 私信列表
- 发送私信
- 统一处理异常
- 统一记录日志



 **第4章
Redis，一站式高性能存储方案** 


- Redis入门
- Spring整合Redis
- 点赞
- 我收到的赞
- 关注、取消关注
- 关注列表、粉丝列表
- 优化登录模块



 **第5章
Kafka，构建TB级异步消息系统** 


- 阻塞队列
- Kafka入门
- Spring整合Kafka
- 发送系统通知
- 显示系统通知



 **第6章
Elasticsearch，分布式搜索引擎** 

- Elasticsearch入门
- Spring整合Elasticsearch
- 开发社区搜索功能


 **第7章
项目进阶，构建安全高效的企业服务** 


- Spring Security
- 权限控制
- 置顶、加精、删除
- Redis高级数据类型
- 网站数据统计
- 任务执行和调度
- 热贴排行
- 生成长图
- 将文件上传至云服务期
- 优化网站的性能



 **第8章
项目发布与总结** 


- 单元测试
- 项目监控
- 项目部署
- 项目总结

