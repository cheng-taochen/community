package com.example.community.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @Author: Ctc
 * @Date: 2022/4/22 12:32
 */
@Configuration
@EnableScheduling
@EnableAsync
public class ThreadPoolConfig {
}

