package com.example.community.util;

import com.example.community.entity.User;
import org.springframework.stereotype.Component;

/**
 * @Author: Ctc
 * @Date: 2022/3/31 21:26
 */

/**
 * 持有用户信息，用于代替session对象
 *
 * @author ctc
 */
@Component
public class HostHodler {
    private ThreadLocal<User> users = new ThreadLocal<>();

    public void setUser(User user) {
        users.set(user);
    }

    public User getUser() {
        return users.get();
    }

    public void clear() {
        users.remove();
    }
}
